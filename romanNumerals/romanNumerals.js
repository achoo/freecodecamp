
//works up to the number 50.

let startSym = ["I", "V", "X", "L"];
let startNum = ["1", "5", "10", "50"];

function convertToRoman(num) {
    let stringNum = num.toString(); //function to convert num argument into a string.
    let splitNum = stringNum.split(''); //function to split stringNum into an array of single digit numbers.
    let romanBuild = []; //an empty array to push numbers into.
    let romanBuild2 = [];
    
    function lessThanFive(arrFive) { //a function that can be used later to calculate numbers that are less than five.
        if (arrFive.length < 2) {
            romanBuild.push(arrFive[arrFive.length - 1]); //if the length of the splitNum array is less than 2, meaning it only holds a single digit number, push it into the empty array.
            let intBuild = parseInt(romanBuild[romanBuild.length - 1]); //convert single digit string number into a integer, which will be used in if statement below.
            let arrForLessThanFive = [];
            if(intBuild < 4) {
                for(let i = 0; i < intBuild; i++) {
                    arrForLessThanFive.push(startSym[0]);
                }
                // console.log(arrForLessThanFive);
                let joinedArr = arrForLessThanFive.join('');
                console.log(joinedArr);
                return joinedArr;
            } else if (intBuild == 4) {
                arrForLessThanFive = ["I", "V"];
                let joinedArr = arrForLessThanFive.join('');
                console.log(joinedArr);
                return joinedArr;
            }
        }
    }
    lessThanFive(splitNum);

    function lessThanTen(arrTen) {
        if (arrTen.length < 2) {
            romanBuild.push(arrTen[arrTen.length - 1]);
            let intBuild = parseInt(romanBuild[romanBuild.length - 1]);
            let arrForLessThanTen = [];
            if(intBuild > 4 && intBuild < 9) {
                arrForLessThanTen.push(startSym[1]);
                // console.log(arrForLessThanTen);
                let numOverFive = intBuild - 5;
                let stringNum2 = numOverFive.toString();
                // console.log(stringNum2);
                let splitNum2 = stringNum2.split('');
                // console.log(splitNum2);
                let arrVPlus = lessThanFive(splitNum2);
                // console.log(arrVPlus);
                let concatArr = arrForLessThanTen.concat(arrVPlus);
                // console.log(concatArr);
                let joinedArr = concatArr.join('');
                console.log(joinedArr);
                return joinedArr;
            } else if (intBuild == 9) {
                arrForLessThanTen = ["I", "X"];
                let joinedArr = arrForLessThanTen.join('');
                console.log(joinedArr);
                return joinedArr
            }
        }
    }
    lessThanTen(splitNum);

    function lessThanFifty(arrFifty) {
        if ((arrFifty.length < 3 && arrFifty.length > 1) && arrFifty[arrFifty.length - 2] < 5) {
            romanBuild.push(arrFifty[arrFifty.length - 2]);
            // console.log(romanBuild);
            let intBuild = parseInt(romanBuild[romanBuild.length - 1]);
            // console.log(intBuild);
            let arrForLessThanFifty = [];
            if(intBuild >= 1 && intBuild < 4) {
                for(let i = 0; i < intBuild; i++) {
                    arrForLessThanFifty.push(startSym[2]);
                }
                // console.log(arrForLessThanFifty);
                let joinedArr = arrForLessThanFifty.join('');
                // console.log(joinedArr);
                romanBuild2.push(arrFifty[arrFifty.length - 1]);
                // console.log(romanBuild2);
                let intBuild2 = parseInt(romanBuild2[romanBuild2.length - 1]);
                // console.log(intBuild2);
                if (intBuild2 > 0 && intBuild2 < 5) {
                    let arrVPlus2 = lessThanFive(romanBuild2);
                    // console.log(arrVPlus2);
                    let concatArr = joinedArr.concat(arrVPlus2);
                    console.log(concatArr);
                    return concatArr;
                } else if(intBuild2 > 4 && intBuild2 < 10) {
                    let arrVPlus2 = lessThanTen(romanBuild2);
                    // console.log(arrVPlus2);
                    let concatArr = joinedArr.concat(arrVPlus2);
                    console.log(concatArr);
                    return concatArr;
                }
            } else if (intBuild == 4) {
                let arrFortyJoin = ["XL"];
                let joinedArr = arrFortyJoin.join('');
                romanBuild2.push(arrFifty[arrFifty.length - 1]);
                // console.log(romanBuild2);
                let intBuild2 = parseInt(romanBuild2[romanBuild2.length - 1]);
                // console.log(intBuild2);
                if (intBuild2 > 0 && intBuild2 < 5) {
                    let arrVPlus2 = lessThanFive(romanBuild2);
                    // console.log(arrVPlus2);
                    let concatArr = joinedArr.concat(arrVPlus2);
                    console.log(concatArr);
                    return concatArr;
                } else if(intBuild2 > 4 && intBuild2 < 10) {
                    let arrVPlus2 = lessThanTen(romanBuild2);
                    // console.log(arrVPlus2);
                    let concatArr = joinedArr.concat(arrVPlus2);
                    console.log(concatArr);
                    return concatArr;
                } else if (intBuild2 == 0) {
                    console.log ("XL");
                    return "XL";
                }
            }
        }
    }
    lessThanFifty(splitNum);

    function isFifty(num) {
        if(num == 50) {
            console.log(startSym[3]);
            return startSym[3];
        }
    }
    isFifty(num);

}

convertToRoman(50);
