function rot13(str) {

    let strSplit = str.split('');
    // console.log(strSplit);
    let decodeArr = [];

    for(let i = 0; i < strSplit.length; i++) {
        var asciiNum = strSplit[i].charCodeAt(0)
        if (asciiNum < 65 || asciiNum > 90) {
            var decodeLetter = String.fromCharCode(asciiNum);
            decodeArr.push(decodeLetter);
        } else if (asciiNum < 78 && asciiNum >= 65) {
            let asciiNumDecode = asciiNum + 13;
            var decodeLetter = String.fromCharCode(asciiNumDecode);
            // console.log(decodeLetter);
            decodeArr.push(decodeLetter);
        } else if (asciiNum > 77 && asciiNum <= 90) {
            let plusSixtyFive = ((asciiNum + 12) - 90) + 65;
            var decodeLetter = String.fromCharCode(plusSixtyFive);
            // console.log(decodeLetter);
            decodeArr.push(decodeLetter);
        }
    }
    let joinedDecode = decodeArr.join('');
    // console.log(joinedDecode);
    return joinedDecode;
}
  
rot13("SERR PBQR PNZC");