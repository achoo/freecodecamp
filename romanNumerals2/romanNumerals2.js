function convertToRoman(num) {
    let romanNums = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
    let modernNums = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];

    let outputString = "";

    for(let i = 0; i < modernNums.length; i++) {
        while(num >= modernNums[i]) {
            outputString = outputString + romanNums[i]
            num = num - modernNums[i];
        }
    }
    // console.log(outputString);
    return outputString;
}
   
convertToRoman(36);

